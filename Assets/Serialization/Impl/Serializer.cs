﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using Serialization;

public static class Serializer
{
    public static ISerializer Current
    {
        get
        {
            if (null == _serializer)
                throw new MissingComponentException();

            return _serializer;
        }

        set
        {
            _serializer = value;
        }
    }

    private static ISerializer _serializer = new UnityJsonSerializer();
}
