﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;

using System.IO;
using System;

namespace Serialization
{
    // NOTE: Doesn't support polymorphic serialization
    public class UnityJsonSerializer : ISerializer
    {
        public string SerializeToString(object data)
        {
            return JsonUtility.ToJson(data, true);
        }
        public string SerializeToString(object data, Type type)
        {
            return JsonUtility.ToJson(data, true);
        }

        public T DeSerializeFromString<T>(string str)
        {
            return JsonUtility.FromJson<T>(str);
        }
        public object DeSerializeFromString(string str, Type t)
        {
            return JsonUtility.FromJson(str, t);
        }

        public void SerializeToFile(object data, string path)
        {
            EnsureDirectory(path);

            using (TextWriter tw = new StreamWriter(path))
            {
                var str = JsonUtility.ToJson(data, true);
                tw.Write(str);
                tw.Flush();
            }
        }
        public void SerializeToFile(object data, Type type, string path)
        {
            SerializeToFile(data, path);
        }

        public T DeSerializeFromFile<T>(string path) where T : class
        {
            return DeSerializeFromFile(path, typeof(T)) as T;
        }
        public object DeSerializeFromFile(string path, Type type)
        {
            object deserializedObj = null;

            using (TextReader rdr = new StreamReader(path))
            {
                var str = rdr.ReadToEnd();
                deserializedObj = JsonUtility.FromJson(str, type);
            }

            return deserializedObj;
        }

        private void EnsureDirectory(string path)
        {
            string dirname = Path.GetDirectoryName(path);
            if (false == Directory.Exists(dirname))
                Directory.CreateDirectory(dirname);
        }
    }
}
