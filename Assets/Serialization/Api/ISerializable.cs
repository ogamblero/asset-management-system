﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

namespace Serialization
{
    public interface ISerializable
    {
        string GetSerialized();
        void SetSerialized(string serialized);
    }
}
