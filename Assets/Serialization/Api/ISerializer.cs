﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using System;

namespace Serialization
{
    public interface ISerializer
    {
        string SerializeToString(object data);
        string SerializeToString(object data, Type type);

        T DeSerializeFromString<T>(string str);
        object DeSerializeFromString(string str, Type t);

        void SerializeToFile(object data, string path);
        void SerializeToFile(object data, Type type, string path);

        T DeSerializeFromFile<T>(string path) where T : class;
        object DeSerializeFromFile(string path, Type type);
    }
}
