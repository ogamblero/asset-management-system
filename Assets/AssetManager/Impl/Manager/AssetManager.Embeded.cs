﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using System.Collections.Generic;

using AssetManagement.Models;

using UnityObject = UnityEngine.Object;

namespace AssetManagement
{
    public partial class AssetManager // Embeded
    {
        private class EmbededAssetManager
        {
            private readonly Dictionary<string, UnityObject> _assetCache = new Dictionary<string, UnityObject>();
            private readonly Dictionary<string, IAssetInfo> _assetInfoCache = new Dictionary<string, IAssetInfo>();

            private AssetManifest _manifest;

            public EmbededAssetManager()
            { }

            public void Initialize(AssetManifest manifest)
            {
                _manifest = manifest;

                for (int i = 0; i < _manifest.Assets.Count; i++)
                {
                    var assetInfo = _manifest.Assets[i];
                    _assetInfoCache.Add(assetInfo.Key, assetInfo);
                }
            }

            public bool Contains(string key)
            {
                return _assetInfoCache.ContainsKey(key);
            }

            public UnityObject Evaluate(string key)
            {
                UnityObject obj;

                if (_assetCache.TryGetValue(key, out obj))
                    return obj;

                IAssetInfo assetInfo;
                if (_assetInfoCache.TryGetValue(key, out assetInfo))
                {
                    obj = Resources.Load(assetInfo.Path);
                    _assetCache.Add(key, obj);
                    return obj;
                }

                return null;
            }

            public void Dispose()
            {
                foreach (var obj in _assetCache.Values)
                    Resources.UnloadAsset(obj as UnityObject);

                _assetCache.Clear();
            }
        }
    }
}