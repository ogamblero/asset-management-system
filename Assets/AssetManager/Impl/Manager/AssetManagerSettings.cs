﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class AssetManagerSettings : ScriptableObject
{
    public string EmbededConfig = "/Resources/asset_configs/emb.txt";
    public string EmbededManifestPath = "/Resources/embeded/emb_manifest.json";

    public string DlcConfig = "/Resources/asset_configs/dlc.txt";
    public string DlcExportPathFormat = "/dlc_export/{0}/";
    public string DlcManifestNameFormal = "dlc_manifest_{0}.json";

    public string AssetBundleExtension = ".bundle";
    public string DefaultAssetBundleName = "ab_main";

    public bool LocalDlc = false;

    public static AssetManagerSettings GetInstance()
    {
        var instance = Resources.Load<AssetManagerSettings>("AssetManagerSettings");
        return instance;
    }

#if UNITY_EDITOR

    [MenuItem("Assets/Create/AssetManagerSettings")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<AssetManagerSettings>();
    }

#endif

}