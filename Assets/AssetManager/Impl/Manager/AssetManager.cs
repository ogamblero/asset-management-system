﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEngine.Assertions;

using System;
using System.Collections.Generic;

using AssetManagement.Models;

using UnityObject = UnityEngine.Object;

namespace AssetManagement
{
    public partial class AssetManager : MonoBehaviour, IAssetManager
    {
        public IAssetGraph Graph { get { return _assetGraph; } }

        private AssetManifest _manifest;
        private AssetGraph _assetGraph;

        private EmbededAssetManager _embededManager;
        private IAssetProviderFactory _assetProviderFactory;

        private readonly Dictionary<ILoadingToken, CallbackInfo> _callbacks;

        /// 
        /// INIT
        /// 

        private AssetManager()
        {
            _callbacks = new Dictionary<ILoadingToken, CallbackInfo>();
        }

        private void Inject(IAssetProviderFactory assetProviderFactory)
        {
            _assetProviderFactory = assetProviderFactory;
        }
        private void Initialize(AssetManifest manifest, AssetManifest embededManifest)
        {
            _manifest = manifest;

            _assetGraph = new AssetGraph();
            _assetGraph.Initialize(_manifest, _assetProviderFactory);

            _embededManager = new EmbededAssetManager();
            _embededManager.Initialize(embededManifest);
        }

        /// 
        /// API
        /// 

        public void Evaluate<T>(string key, Action<string, T> callback) where T : UnityObject
        {
            if (callback == null)
                throw new ArgumentNullException("Callback is NULL");

            T obj;
            if (TryEvaluate<T>(key, out obj))
            {
                callback(key, obj);
                return;
            }

            var token = Load(key);
            _callbacks.Add(token, new CallbackInfo() { Key = key, callback = callback, Type = typeof(T) });

            token.StateChanged += Token_StateChanged;
            Token_StateChanged(token);
        }
        public bool TryEvaluate<T>(string key, out T obj) where T : UnityObject
        {
            if (false == _assetGraph.Contains(key))
            {
                obj = _embededManager.Evaluate(key) as T;
                return true;
            }

            var node = _assetGraph.GetNode(key);
            if (node.State == NodeStates.Loaded)
            {
                obj = ResolveObject(node.Value, typeof(T)) as T;
                return true;
            }

            obj = null;
            return false;
        }

        public ILoadingToken Load(string key)
        {
            return Load(new List<string>() { key });
        }

        public ILoadingToken Load(IEnumerable<string> keys)
        {
            List<AssetNode> nodes = new List<AssetNode>();
            foreach (var key in keys)
            {
                var node = _assetGraph.GetNodeInternal(key);
                if (node != null)
                    nodes.Add(node);
            }

            var token = new LoadingToken(nodes);
            return token;
        }

        /// 
        /// HANDLERS
        /// 

        private void Token_StateChanged(ILoadingToken token)
        {
            if (token.State == LoadingStates.Failured || token.State == LoadingStates.Succeed)
            {
                var info = _callbacks[token];
                _callbacks.Remove(token);

                if (token.State == LoadingStates.Succeed)
                {
                    var node = _assetGraph.GetNode(info.Key);
                    Assert.IsNotNull(node.Value);
                    info.callback.DynamicInvoke(info.Key, node.Value);
                    return;
                }

                if (token.State == LoadingStates.Failured)
                {
                    var obj = _embededManager.Evaluate(info.Key);
                    info.callback.DynamicInvoke(info.Key, obj);
                    return;
                }
            }
        }

        /// 
        /// UTILS
        /// 

        private static UnityObject ResolveObject(UnityObject obj, Type t)
        {
            Type objType = obj.GetType();

            if (t.IsAssignableFrom(objType))
            {
                return obj;
            }
            if (typeof(Component).IsAssignableFrom(t))
            {
                if (obj is Component)
                    return (obj as Component).GetComponent(t);
                if (obj is GameObject)
                    return (obj as GameObject).GetComponent(t);
            }
            if (t == typeof(GameObject) && obj is Component)
            {
                return (obj as Component).gameObject;
            }
            if (t == typeof(Sprite) && obj is Texture2D)
            {
                var t2d = (Texture2D) obj;
                return Sprite.Create(t2d, new Rect(0f, 0f, t2d.width, t2d.height), new Vector2(0.5f, 0.5f));
            }

            return null;
        }

        /// 
        /// FACTORIES
        /// 

        public static AssetManager Create(AssetManifest manifest, AssetManifest embededManifest, IAssetProviderFactory assetProviderFactory)
        {
            AssetManager manager = new GameObject("_assetManager").AddComponent<AssetManager>();

            manager.Inject(assetProviderFactory);
            manager.Initialize(manifest, embededManifest);
            DontDestroyOnLoad(manager.gameObject);

            return manager;
        }

        ///
        /// TYPES
        ///

        private class CallbackInfo
        {
            public Delegate callback;
            public string Key;
            public Type Type;
        }

        /// 
        /// TYPES
        /// 

        private class LoadingToken : ILoadingToken
        {
            public event Action<ILoadingToken> StateChanged;

            public LoadingStates State
            {
                get { return _state; }
                private set
                {
                    if (_state == value)
                        return;

                    _state = value;

                    if (null != StateChanged)
                        StateChanged(this);
                }
            }
            public float Progress
            {
                get
                {
                    float progress = 0.0f;
                    for (int i = 0; i < _nodes.Count; i++)
                        progress += _nodes[i].LoadingProgress;

                    progress /= _nodes.Count;

                    return progress;
                }
            }

            private LoadingStates _state;
            private List<AssetNode> _nodes;

            public LoadingToken(List<AssetNode> nodes)
            {
                _nodes = nodes;
                State = LoadingStates.Running;

                for (int i = 0; i < _nodes.Count; i++)
                    _nodes[i].LoadAsync(OnNodeLoadingCallback);
            }

            public void Cancel()
            {
                Assert.IsTrue(State == LoadingStates.Running);
                State = LoadingStates.Failured;
            }
            public void Retry()
            {
                Assert.IsTrue(State == LoadingStates.Failured);
                State = LoadingStates.Running;
            }

            private void OnNodeLoadingCallback(AssetNode node)
            {
                for (int i = 0; i < _nodes.Count; i++)
                {
                    var temp = _nodes[i];
                    if (temp.State != NodeStates.Loaded)
                    {
                        Assert.IsTrue(temp.State == NodeStates.Failed);
                        State = LoadingStates.Failured;
                        return;
                    }
                }

                State = LoadingStates.Succeed;
            }
        }
    }
}
