﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEngine.Assertions;

using System;
using System.Collections.Generic;
using System.Linq;

using AssetManagement.Models;
using Observable.Requests;

using UnityObject = UnityEngine.Object;

namespace AssetManagement
{
    public partial class AssetManager // Graph
    {
        private class AssetGraph : IAssetGraph
        {
            public static AssetGraph Current { get; private set; }

            public IEnumerable<IAssetNode> Nodes
            {
                get { return _nodesCache.Values.Cast<IAssetNode>(); }
            }

            private readonly Dictionary<AssetInfo, AssetNode> _nodesCache = new Dictionary<AssetInfo, AssetNode>();
            private readonly Dictionary<string, AssetInfo> _assetsCache = new Dictionary<string, AssetInfo>();

            public IAssetProviderFactory ProviderFactory { get; private set; }

            public void Initialize(AssetManifest manifest, IAssetProviderFactory providerFactory)
            {
                Current = this;
                ProviderFactory = providerFactory;

                _assetsCache.Clear();
                _nodesCache.Clear();

                foreach (var assetInfo in manifest.Assets)
                {
                    _assetsCache.Add(assetInfo.key, assetInfo);
                    _nodesCache.Add(assetInfo, new AssetNode(assetInfo));
                }

                foreach (var assetInfo in manifest.Assets)
                {
                    AssetNode node;
                    _nodesCache.TryGetValue(assetInfo, out node);

                    for (int i = 0; i < assetInfo.deps.Count; i++)
                    {
                        var depKey = assetInfo.deps[i];
                        AssetInfo dep;
                        _assetsCache.TryGetValue(depKey, out dep);

                        if (dep == null)
                        {
                            Debug.LogErrorFormat("Can't resolve {0} dependency for {1}", depKey, assetInfo.key);
                            continue;
                        }

                        AssetNode depNode;
                        _nodesCache.TryGetValue(dep, out depNode);

                        node.AddDependency(depNode);
                        depNode.AddDepended(node);
                    }
                }
            }

            public IAssetNode GetNode(string key)
            {
                return GetNodeInternal(key);
            }

            public bool Contains(string key)
            {
                return _assetsCache.ContainsKey(key);
            }

            public AssetNode GetNodeInternal(string key)
            {
                AssetInfo assetInfo = null;
                _assetsCache.TryGetValue(key, out assetInfo);

                if (assetInfo == null)
                    return null;

                return _nodesCache[assetInfo];
            }
        }

        ///
        /// ASSET_NODE
        ///

        private class AssetNode : IAssetNode
        {
            public float LoadingProgress
            {
                get
                {
                    if (Value != null)
                        return 1.0f;

                    float progress = 0.0f;
                    if (_request != null)
                        progress += _request.Progress;

                    int count = 1;
                    if (_dependencies != null)
                    {
                        for (int i = 0; i < _dependencies.Count; i++)
                            progress += _dependencies[i].LoadingProgress;

                        count += _dependencies.Count;
                    }

                    return progress / count;
                }
            }

            public NodeStates State
            {
                get
                {
                    Assert.IsFalse(_state == NodeStates.Loaded && Value == null);
                    return _state;
                }
                private set
                {
                    _state = value;
                }
            }

            public UnityObject Value { get; private set; }
            public IAssetInfo AssetInfo { get { return _assetInfo; } }

            public IEnumerable<IAssetNode> Dependencies { get { return _dependencies.Cast<IAssetNode>(); } }
            public IEnumerable<IAssetNode> Depended { get { return _depended.Cast<IAssetNode>(); } }

            private AssetInfo _assetInfo;
            private List<AssetNode> _dependencies;
            private List<AssetNode> _depended;

            private AssetRequest _request;
            private List<Action<AssetNode>> _callbacks;

            private NodeStates _state;

            public AssetNode(AssetInfo assetInfo)
            {
                _assetInfo = assetInfo;
            }

            /// 
            /// API
            ///

            public void LoadAsync(Action<AssetNode> callback)
            {
                if (State == NodeStates.Loaded)
                {
                    if (callback != null)
                        callback(this);
                    return;
                }

                if (callback != null)
                {
                    if (_callbacks == null)
                        _callbacks = new List<Action<AssetNode>>();

                    _callbacks.Add(callback);
                }

                if (State != NodeStates.Loading)
                {
                    if (_dependencies != null)
                    {
                        for (int i = 0; i < _dependencies.Count; i++)
                            _dependencies[i].LoadAsync(OnDepLoadedHandler);
                    }
                    else
                    {
                        _request = AssetGraph.Current.ProviderFactory.CreateProvider(_assetInfo, AssetGraph.Current);
                        _request.Execute(OnRequestFinishedHandler);
                    }

                    State = NodeStates.Loading;
                }
            }

            public void Unload()
            {
                if (State == NodeStates.Idle || State == NodeStates.Failed)
                    return;

                var tempState = State;
                State = NodeStates.Failed;

                for (int i = 0; _depended != null && i < _depended.Count; i++)
                {
                    if (_depended[i].State == NodeStates.Loading || _depended[i].State == NodeStates.Loaded)
                    {
                        State = tempState;
                        return;
                    }
                }

                for (int j = 0; j < _callbacks.Count; j++)
                    _callbacks[j](this);
                _callbacks = null;

                // NOTE: Destroy _dependencies firstly.
                for (int i = 0; _dependencies != null && i < _dependencies.Count; i++)
                    _dependencies[i].Unload();

                if (Value != null)
                    Destroy(Value);
            }

            /// 
            /// HANDLERS
            /// 

            private void OnDepLoadedHandler(AssetNode dep)
            {
                if (State == NodeStates.Failed)
                    return;

                for (int i = 0; i < _dependencies.Count; i++)
                {
                    var temp = _dependencies[i];
                    if (temp.State == NodeStates.Failed)
                    {
                        // NOTE: What to do with loaded assets ??? Do nothing. Client side should handle it.
                        State = NodeStates.Failed;
                        for (int j = 0; j < _callbacks.Count; j++)
                            _callbacks[j](this);
                        _callbacks = null;
                        return;
                    }
                    if (temp.State != NodeStates.Loaded)
                    {
                        Assert.IsTrue(temp.State == NodeStates.Loading);
                        return;
                    }
                }

                _request = AssetGraph.Current.ProviderFactory.CreateProvider(_assetInfo, AssetGraph.Current);
                _request.Execute(OnRequestFinishedHandler);
            }

            private void OnRequestFinishedHandler(Request sender)
            {
                Assert.AreEqual(_request, sender);

                AssetRequest request = sender as AssetRequest;

                if (request.TheResponse != null && request.TheResponse.Type == Response.ResponseTypes.Success)
                    Value = request.TheResponse.Data;

                request.Dispose();
                _request = null;

                State = Value != null ? NodeStates.Loaded : NodeStates.Failed;

                for (int i = 0; i < _callbacks.Count; i++)
                    _callbacks[i](this);

                _callbacks = null;
            }

            /// 
            /// INTERNAL
            /// 

            public void AddDependency(AssetNode node)
            {
                if (_dependencies == null)
                    _dependencies = new List<AssetNode>();

                _dependencies.Add(node);
            }

            public void AddDepended(AssetNode node)
            {
                if (_depended == null)
                    _depended = new List<AssetNode>();

                _depended.Add(node);
            }
        }
    }
}
