﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using Observable.Requests;

namespace AssetManagement
{
    public abstract class AssetRequest : Request<Object>
    {
        protected IAssetInfo _assetInfo;
        protected IAssetGraph _assetGraph;

        public AssetRequest(IAssetInfo assetInfo, IAssetGraph assetGraph)
        {
            _assetInfo = assetInfo;
            _assetGraph = assetGraph;
        }
    }
}
