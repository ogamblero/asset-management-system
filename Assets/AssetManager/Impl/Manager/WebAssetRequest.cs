﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;

namespace AssetManagement.Requests
{
    public abstract class WebAssetRequest : AssetRequest
    {
        public const float TIMEOUT = 15.0f;

        private WWW _www;
        protected byte[] _bytes;

        private float _lastUpdateTime;
        private float _lastProgerss;

        public override float Progress
        {
            get
            {
                return ((null != _www) ? _www.progress : 1.0f);
            }
        }

        public WebAssetRequest(IAssetInfo assetInfo, IAssetGraph assetGraph, string url)
            : base (assetInfo, assetGraph)
        {
            int cachedHash = PersistantStorage.Current.ContainsVersion(assetInfo.Key);

            if (cachedHash != assetInfo.Hash)
                _www = new WWW(url);
            else
                _bytes = PersistantStorage.Current.Load(assetInfo.Key);

            _lastProgerss = 0.0f;
            _lastUpdateTime = Time.timeSinceLevelLoad;
        }

        protected override bool OnUpdate()
        {
            if (null == _www)
                return true;

            if (true == _www.isDone)
            {
                if (true == string.IsNullOrEmpty(_www.error))
                {
                    _bytes = _www.bytes;
                    PersistantStorage.Current.Save(_assetInfo.Key, _bytes, _assetInfo.Hash);
                }
                else
                {
                    Debug.LogError(_www.url + " : " + _www.error);
                }

                _www.Dispose();
                _www = null;
                return true;
            }

            if (Mathf.Abs(_www.progress - _lastProgerss) > 0.0001f)
            {
                _lastProgerss = _www.progress;
                _lastUpdateTime = Time.timeSinceLevelLoad;
            }

            if ((Time.timeSinceLevelLoad - _lastUpdateTime) > TIMEOUT)
            {
                _www.Dispose();
                _www = null;
                return true;
            }

            return false;
        }

        protected override void OnDispose()
        {
            if (null != _www)
            {
                _www.Dispose();
                _www = null;
            }
        }
    }
}