﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEditor;
using AssetManagement;

namespace AssetManagementEditor
{
    [CustomEditor(typeof(AssetManager))]
    public class AssetManagerInspector : Editor
    {
        private string _filter;
        private IAssetGraph _graph;

        public override void OnInspectorGUI()
        {
            if (_graph == null)
                _graph = (target as AssetManager).Graph;

            _filter = EditorGUILayout.TextField("Filter", _filter);

            EditorGUILayout.Space();

            foreach (var node in _graph.Nodes)
            {
                EditorGUILayout.BeginHorizontal();
                {
                    var key = node.AssetInfo.Key;
                    var obj = node.Value;

                    if (string.IsNullOrEmpty(_filter) || false == string.IsNullOrEmpty(_filter) && key.Contains(_filter))
                    {
                        if (obj != null)
                            EditorGUILayout.ObjectField(key, obj, obj.GetType(), true);
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
        }
    }
}
