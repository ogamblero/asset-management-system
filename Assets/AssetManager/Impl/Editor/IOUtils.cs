﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;

using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace AssetManagementEditor
{
    public class IOUtils
    {
        public static IEnumerable<string> GetFileNames(string path)
        {
            Queue<string> queue = new Queue<string>();
            queue.Enqueue(path);
            while (queue.Count > 0)
            {
                path = queue.Dequeue();
                try
                {
                    foreach (string subDir in Directory.GetDirectories(path))
                    {
                        queue.Enqueue(subDir);
                    }
                }
                catch (System.Exception ex)
                {
                    Debug.LogError(ex.Message);
                }
                string[] files = null;
                try
                {
                    files = Directory.GetFiles(path);
                }
                catch (System.Exception ex)
                {
                    Debug.LogError(ex.Message);
                }
                if (files != null)
                {
                    for (int i = 0; i < files.Length; i++)
                    {
                        yield return files[i];
                    }
                }
            }
        }


        public static string[] GetDirectories(string path, int depth)
        {
            string[] dirs = Directory.GetDirectories(path);

            var result = dirs.Select(dir =>
            {
                int lastIndex = dir.LastIndexOf('\\') + 1;
                return dir.Substring(lastIndex, dir.Length - lastIndex);
            });

            return result.ToArray();
        }

    }
}
