﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEditor;

using System;

namespace AssetManagementEditor
{
	public class TextPopupEditor : EditorWindow
	{
		private static string _value;
		private static Action<string> _callback;

		public static void Show(string value, Action<string> callback)
		{
			_value = value;
			_callback = callback;

			TextPopupEditor window = (TextPopupEditor)GetWindow(typeof(TextPopupEditor));
            window.titleContent.text = "TextPopupEditor";
			window.Show();
		}

		private void OnGUI()
		{
			DrawEditor();
		}

		private void DrawEditor()
		{
			EditorGUILayout.BeginVertical();
			{
				_value = EditorGUILayout.TextField("Value", _value);

				EditorGUILayout.Space();
				EditorGUILayout.Space();
				EditorGUILayout.Space();

				EditorGUILayout.BeginHorizontal();
				{
					if(GUILayout.Button("Ok"))
					{
						if(_callback != null)
						{
							_callback(_value);
							_callback = null;
						}
						Close();
					}

					if(GUILayout.Button("Cancel"))
					{
						Close();
					}
				}
				EditorGUILayout.EndHorizontal();

			}
			EditorGUILayout.EndVertical();
		}
	}
}
