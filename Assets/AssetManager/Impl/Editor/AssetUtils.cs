﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEditor;

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace AssetManagementEditor
{
    public static class AssetUtils
    {
        private static HashAlgorithm _hashAlgorithm;
        private static HashAlgorithm HashAlgorithm
        {
            get
            {
                if (_hashAlgorithm == null)
                    _hashAlgorithm = MD5.Create();

                return _hashAlgorithm;
            }
        }

        public static string ComputeHash(UnityEngine.Object asset, bool deps = true)
        {
            string fName = AssetDatabase.GetAssetPath(asset);

            List<string> names = new List<string>();

            if (true == deps)
                names = GetGenericDependencies(new string[1] { fName });

            names.Add(fName);
            names.Sort((i1, i2) => i1.CompareTo(i2));

            StringBuilder sb = new StringBuilder();
            foreach (var name in names)
            {
                using (FileStream fs = new FileStream(name, FileMode.Open))
                {
                    byte[] byteHash = HashAlgorithm.ComputeHash(fs);
                    sb.AppendLine(Encoding.UTF8.GetString(byteHash));
                }
            }

            string fHash = sb.ToString();
            return fHash;
        }

        // TODO: Improve logic
        public static List<string> GetGenericDependencies(string[] assets, bool recursive = false)
        {
            List<string> result = AssetDatabase.GetDependencies(assets, recursive)
                .Where(name => (!name.EndsWith(".cs") && !name.EndsWith(".shader")))
                .Where(name => !assets.Contains(name))
                .Select(name => name)
                .ToList();

            return result;
        }

        public static RuntimePlatform GetRuntimePlatform(BuildTarget buildTarget)
        {
            switch (buildTarget)
            {
                case BuildTarget.Android:
                    return RuntimePlatform.Android;
                case BuildTarget.iOS:
                    return RuntimePlatform.IPhonePlayer;
            }

            throw new NotSupportedException();
        }

        private static string GetResourcePathWithExtension(UnityEngine.Object obj)
        {
            string path = null;
            string assetPath = AssetDatabase.GetAssetPath(obj);

            if (!string.IsNullOrEmpty(assetPath))
            {
                string resFolder = "Resources";
                int index = assetPath.IndexOf(resFolder);

                if (index >= 0)
                {
                    index += (resFolder.Length + 1);
                    int length = assetPath.Length - index;

                    path = assetPath.Substring(index, length);

                    return path;
                }
            }

            return null;
        }

        public static string GetResourcePath(UnityEngine.Object obj)
        {
            string path = null;
            string assetPath = AssetDatabase.GetAssetPath(obj);

            if (!string.IsNullOrEmpty(assetPath))
            {
                string resFolder = "Resources";
                int index = assetPath.IndexOf(resFolder);

                if (index >= 0)
                {
                    index += (resFolder.Length + 1);
                    int length = assetPath.LastIndexOf('.') - index;

                    path = assetPath.Substring(index, length);

                    return path;
                }
            }

            return null;
        }
    }
}