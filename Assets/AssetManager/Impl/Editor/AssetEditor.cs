﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEditor;

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using AssetManagement.Models;

using UnityObject = UnityEngine.Object;
using UnityEngine.Assertions;

namespace AssetManagementEditor
{
	public class AssetEditor : EditorWindow
	{

        private BuildTarget _buildTarget;
		private List<EditorAssetInfo> _editorAssetsInfo;

		private AssetProviderTypes _assetsType;
		private Vector2 _scroll;

        private string _filterStr = null;

        private AssetManagerSettings _settings;

        [MenuItem("Systems/AssetManagement/Asset Editor")]
		private static void Init()
		{
			AssetEditor window = (AssetEditor)EditorWindow.GetWindow(typeof(AssetEditor));
			window.Show();
		}

		private void OnGUI()
		{
            if (_settings == null)
                _settings = AssetManagerSettings.GetInstance();

            if (null == _editorAssetsInfo)
				_editorAssetsInfo = new List<EditorAssetInfo> ();

			_assetsType = (AssetProviderTypes)EditorGUILayout.EnumPopup("AssetType", _assetsType);

            if (AssetProviderTypes.Dlc == _assetsType)
            {
                _buildTarget = EditorUserBuildSettings.activeBuildTarget;
                EditorGUILayout.LabelField("Target: ", _buildTarget.ToString());
            }

			EditorGUILayout.BeginHorizontal();
			{
				if(true == GUILayout.Button("Import"))
				{
                    string configPath;
                    if (_assetsType == AssetProviderTypes.Embeded)
                        configPath = Application.dataPath + _settings.EmbededConfig;
                    else
                        configPath = Application.dataPath + _settings.DlcConfig;

                    var importConfig = AssetsConfig.Load(configPath);
                    if (null == importConfig)
                    {
                        Debug.LogErrorFormat("Can't load {0} config at {1}", _assetsType, configPath);
                        return;
                    }

                    _editorAssetsInfo = ImportAssets(importConfig);
                }

				if(true == GUILayout.Button("Export"))
				{
					if(_editorAssetsInfo.Any(r => r.IsConflicted))
					{
						Debug.LogError("Can't build manifest. Please, resolve conflicts.");
						return;
					}

                    if (AssetProviderTypes.Embeded == _assetsType)
                    {
                        try
                        {
                            var embManifestPath = Application.dataPath + _settings.EmbededManifestPath;
                            var manifest = CreateEmbededManifest(_editorAssetsInfo);
                            Serializer.Current.SerializeToFile(manifest, embManifestPath);
                        }
                        catch (Exception e)
                        {
                            EditorUtility.ClearProgressBar();
                            throw e;
                        }
                    }
                    else if (AssetProviderTypes.Dlc == _assetsType)
                    {
                        var platform = AssetUtils.GetRuntimePlatform(_buildTarget);

                        string exportPath = Application.dataPath/*.Replace("Assets", "")*/ + string.Format(_settings.DlcExportPathFormat, platform);
                        var dlcManifestPath = exportPath + string.Format(_settings.DlcManifestNameFormal, platform);

                        try
                        {
                            var manifest = BuildDlcAssets(_editorAssetsInfo, exportPath);
                            Serializer.Current.SerializeToFile(manifest, dlcManifestPath);
                        }
                        catch (Exception e)
                        {
                            EditorUtility.ClearProgressBar();
                            throw e;
                        }
                    }

					AssetDatabase.Refresh();
				}
            }
            EditorGUILayout.EndHorizontal();

			_scroll = EditorGUILayout.BeginScrollView(_scroll);
			{
				EditorGUILayout.BeginVertical();
				{
					EditorGUI.indentLevel++;
					foreach(var resource in _editorAssetsInfo)
					{
						if(string.IsNullOrEmpty(_filterStr) ||
						   (!string.IsNullOrEmpty(resource.Key) && resource.Key.ToLowerInvariant().Contains(_filterStr.ToLowerInvariant())))
						{
							OnGuiAsset(resource.Key, resource, _editorAssetsInfo);
						}
					}
				}
				EditorGUILayout.EndVertical();
			}
			EditorGUILayout.EndScrollView();
		}

        /// 
        /// INTERNAL
        /// 

        private List<EditorAssetInfo> ImportAssets(AssetsConfig config)
		{
			var eAssetsInfo = new List<EditorAssetInfo> ();

			foreach(var congifItem in config.Items)
			{
                var rootPath = Application.dataPath + congifItem.Path;

				var assetFiles = IOUtils.GetFileNames(rootPath)
                    .Where(s => !s.Contains(".meta") && (!s.Contains(".DS_Store")))
                    .ToArray();

				for(int i = 0; i < assetFiles.Length; i++)
				{
					string assetPath = assetFiles[i];

					EditorAssetInfo eAssetInfo = null;
					do
					{
                        eAssetInfo = ImportAsset(assetPath);
                        if (false == eAssetInfo.Value)
						{
							string msg = "Can't load " + eAssetInfo.Key + " at " + assetPath + ". Please, reimport asset.";
							EditorUtility.DisplayDialog("Import error", msg, "Reimport");
						}

					} while(false == eAssetInfo.Value);

                    Assert.IsNotNull(eAssetInfo);

                    // Is Conflicted
                    var conflictedList = eAssetsInfo.FindAll(r => r.Key == eAssetInfo.Key);
                    if (conflictedList.Count > 0)
                    {
                        eAssetInfo.IsConflicted = true;
                        conflictedList.ForEach(r => r.IsConflicted = true);
                    }

                    eAssetInfo.AssetBundleName = congifItem.AssetBundle;
                    eAssetsInfo.Add(eAssetInfo);
                }
			}

            ProcessDependencies(eAssetsInfo);

			return eAssetsInfo;
		}

        private static void ProcessDependencies(List<EditorAssetInfo> assetsInfo)
        {
            foreach (var eAssetInfo in assetsInfo)
            {
                string assetPath = AssetDatabase.GetAssetPath(eAssetInfo.Value);
                // NOTE: Ignore scripts and shaders
                List<string> genericDeps = AssetUtils.GetGenericDependencies(new string[1] { assetPath });

                foreach (var dep in genericDeps)
                {
                    UnityObject asset = AssetDatabase.LoadAssetAtPath<UnityObject>(dep);
                    var depAsset = assetsInfo.Find(r => r.Value == asset);
                    if (null != depAsset)
                    {
                        eAssetInfo.Deps.Add(depAsset);
                    }
                }
            }
        }

        private static EditorAssetInfo ImportAsset(string path)
        {
            var eAssetInfo = new EditorAssetInfo();

            eAssetInfo.Key = Path.GetFileNameWithoutExtension(path);

            var assetPath = path.Substring(path.IndexOf("Assets"));
            eAssetInfo.Value = AssetDatabase.LoadAssetAtPath(assetPath, typeof(UnityObject));

            var hashStr = AssetUtils.ComputeHash(eAssetInfo.Value, true);
            eAssetInfo.Hash = hashStr.GetHashCode();

            return eAssetInfo;
        }

        private static AssetManifest CreateEmbededManifest(List<EditorAssetInfo> eAssetsInfo)
        {
            AssetManifest manifest = new AssetManifest();

            foreach (var eAssetInfo in eAssetsInfo)
            {
                var assetInfo = new AssetInfo();

                assetInfo.key = eAssetInfo.Key;
                assetInfo.path = AssetUtils.GetResourcePath(eAssetInfo.Value);
                assetInfo.hash = eAssetInfo.Hash;
                assetInfo.deps = null;

                manifest.Assets.Add(assetInfo);
            }

            return manifest;
        }

        private AssetManifest BuildDlcAssets(List<EditorAssetInfo> eAssetsInfo, string exportPath)
		{
            var manifest = OnPreBuildAssets(eAssetsInfo, exportPath);
            BuildPipeline.BuildAssetBundles(exportPath, BuildAssetBundleOptions.None, _buildTarget);
            OnPostBuildAssets(manifest, exportPath);

            return manifest;
		}

        protected AssetManifest OnPreBuildAssets(List<EditorAssetInfo> eAssetsInfo, string exportPath)
        {
            AssetManifest manifest = new AssetManifest();

            float count = eAssetsInfo.Count;
            int index = 0;

            // Assets processing...
            var abNames = new HashSet<string>();

            foreach (var eAssetInfo in eAssetsInfo)
            {
                EditorUtility.DisplayProgressBar("Exporting assets...", eAssetInfo.Key, index / count);

                var assetInfo = new AssetInfo();

                assetInfo.key = eAssetInfo.Key;
                assetInfo.path = string.Format("ab://{0}/{1}", eAssetInfo.AssetBundleName, eAssetInfo.Key);
                assetInfo.hash = eAssetInfo.Hash;
                assetInfo.deps = eAssetInfo.Deps.Select(i => i.Key).ToList();

                if (false == string.IsNullOrEmpty(eAssetInfo.AssetBundleName))
                {
                    assetInfo.deps.Add(eAssetInfo.AssetBundleName);
                    if (false == abNames.Contains(eAssetInfo.AssetBundleName))
                        abNames.Add(eAssetInfo.AssetBundleName);
                }

                manifest.Assets.Add(assetInfo);
                index++;
            }

            // AssetBundles processing...
            foreach (var eai in eAssetsInfo)
            {
                if (eai.Value == null)
                    continue;

                var path = AssetDatabase.GetAssetPath(eai.Value);
                var importer = AssetImporter.GetAtPath(path);

                var abName = string.IsNullOrEmpty(eai.AssetBundleName) ? 
                    string.Concat(_settings.DefaultAssetBundleName, _settings.AssetBundleExtension) :
                    string.Concat(eai.AssetBundleName, _settings.AssetBundleExtension);

                importer.assetBundleName = abName;
            }

            foreach (var abName in abNames)
            {
                var abAssetInfo = new AssetInfo();
                abAssetInfo.key = abName;
                abAssetInfo.path = abName + _settings.AssetBundleExtension;

                manifest.Assets.Add(abAssetInfo);
            }

            EditorUtility.ClearProgressBar();

            // Directory cleanup...
            if (true == Directory.Exists(exportPath))
                Directory.Delete(exportPath, true);

            if (false == Directory.Exists(exportPath))
                Directory.CreateDirectory(exportPath);

            return manifest;
        }
        protected void OnPostBuildAssets(AssetManifest manifest, string exportPath)
        {
            var manifestFiles = IOUtils.GetFileNames(exportPath).Where(s => (true == s.EndsWith(".manifest"))).ToArray();
            foreach (var filename in manifestFiles)
                File.Delete(filename);

            var platform = AssetUtils.GetRuntimePlatform(_buildTarget);
            var platformBundleName = IOUtils.GetFileNames(exportPath).Where(s => (s.EndsWith(platform.ToString()))).First();
            File.Delete(platformBundleName);
        }

        /// 
        /// GUI
        /// 

        private void OnGuiAsset(string key, EditorAssetInfo eassetInfo, List<EditorAssetInfo> resources)
		{
			EditorGUILayout.BeginHorizontal();
			{
				var color = GUI.color;
				if(eassetInfo.IsConflicted)
				{
					GUI.color = Color.red;
					EditorGUILayout.LabelField(":=(", GUILayout.Width(42), GUILayout.MaxWidth(42));
					GUI.color = Color.white;
				}

				GUI.color = color;

				EditorGUILayout.ObjectField(key, eassetInfo.Value, typeof(UnityObject), false);

                GUI.enabled = eassetInfo.Value != null;
                if (GUILayout.Button("Update", GUILayout.Width(64)))
				{
					OnGuiUpdateAsset(key, eassetInfo, resources);
					return;
				}
                GUI.enabled = true;

            }
			EditorGUILayout.EndHorizontal();
		}

		private void OnGuiUpdateAsset(string key, EditorAssetInfo resource, List<EditorAssetInfo> container)
		{
			Action<string> okAction = value =>
			{
				if(null != container.Find(res => res.Key == value))
				{
					EditorUtility.DisplayDialog("Error",
						"Dictionary has already contained " + value +
						" resource!", "Ok");
					return;
				}

				var conflicted = container.FindAll(res => res.Key == key);
				conflicted.ForEach(res => res.IsConflicted = false);
				resource.Key = value;

				// Move asset
				string oldPath = AssetDatabase.GetAssetPath(resource.Value);
				int index = oldPath.LastIndexOf(key);
				string newPath = oldPath.Substring(0, index) + value + oldPath.Substring(index + key.Length, oldPath.Length - index - key.Length);
				AssetDatabase.MoveAsset(oldPath, newPath);
				Debug.LogFormat("Asset {0} moved to {1}", oldPath, newPath);
			};

			TextPopupEditor.Show(key, okAction);
		}

        /// 
        /// TYPES
        /// 

        private enum AssetProviderTypes
        {
            Embeded,
            Dlc
        }

        private class AssetsConfig
		{
			public List<AssetsConfigItem> Items { get; private set; }

            public class AssetsConfigItem
            {
                public string Path { get; set; }
                public string AssetBundle { get; set; }
            }

            public static AssetsConfig Load(string path)
            {
                AssetsConfig config = new AssetsConfig();

                config.Items = new List<AssetsConfigItem>();
                using (TextReader rdr = new StreamReader(path))
                {
                    string line = null;
                    while ((line = rdr.ReadLine()) != null)
                    {
                        string[] args = line.Split(' ');
                        var ci = new AssetsConfigItem();
                        ci.Path = args[0];
                        if (args.Length > 1)
                            ci.AssetBundle = args[1];

                        config.Items.Add(ci);
                    }
                }

                return config;
            }
        }

        protected class EditorAssetInfo
		{
			public string Key { get; set; }
            public int Hash { get; set; }

            public UnityObject Value { get; set; }
            public List<EditorAssetInfo> Deps { get; set; }
            public string AssetBundleName { get; set; }

            public bool IsConflicted { get; set; }

			public EditorAssetInfo()
			{
				Deps = new List<EditorAssetInfo>();
			}
		}
	}
}