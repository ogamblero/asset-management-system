﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using System.IO;

#if UNITY_EDITOR

using UnityEditor;

namespace AssetManagementEditor
{
    public static class StorageUtils
    {
        [MenuItem("Systems/Storage/Open persistent storage location")]
        private static void OpenPersistentStorage()
        {
            EditorUtility.RevealInFinder(Application.persistentDataPath);
        }

        [MenuItem("Systems/Storage/Open temp folder location")]
        private static void OpenTemptStorage()
        {
            EditorUtility.RevealInFinder(Application.temporaryCachePath);
        }

        [MenuItem("Systems/Storage/Clear storage")]
        private static void ClearAllStorage()
        {
            ClearFolder(Application.persistentDataPath);
            ClearFolder(Application.temporaryCachePath);
            PlayerPrefs.DeleteAll();
        }

        private static void ClearFolder(string folderName)
        {
            var dir = new DirectoryInfo(folderName);

            foreach (DirectoryInfo di in dir.GetDirectories())
                di.Delete(true);

            foreach (FileInfo fi in dir.GetFiles())
                fi.Delete();
        }
    }
}

#endif