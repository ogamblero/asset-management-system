﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using Observable.Requests;

namespace AssetManagement.Requests
{
    public class AssetBundleRequest : WebAssetRequest
    {
        public AssetBundle AssetBundle { get; private set; }

        public AssetBundleRequest(IAssetInfo assetInfo, IAssetGraph assetGraph, string url)
            : base(assetInfo, assetGraph, url)
        { }

        protected override bool OnUpdate()
        {
            bool isDone = base.OnUpdate();

            if (isDone)
            {
                if (null != _bytes)
                {
                    AssetBundle = AssetBundle.LoadFromMemory(_bytes);
                    TheResponse = new Response<Object>() { Data = AssetBundle };
                }
            }

            return isDone;
        }

        protected override void OnDispose()
        {
            base.OnDispose();
            AssetBundle = null;
        }
    }

    public class LocalAssetBundleRequest : AssetRequest
    {
        public AssetBundle AssetBundle { get; private set; }
        private string _path;

        public LocalAssetBundleRequest(IAssetInfo assetInfo, IAssetGraph assetGraph, string path)
            : base(assetInfo, assetGraph)
        {
            _path = path;
        }

        protected override bool OnUpdate()
        {
            AssetBundle = AssetBundle.LoadFromFile(_path);
            TheResponse = new Response<Object>() { Data = AssetBundle };
            return true;
        }

        protected override void OnDispose()
        {
            base.OnDispose();
            AssetBundle = null;
        }
    }
}