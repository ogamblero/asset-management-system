﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using Observable.Requests;

namespace AssetManagement.Requests
{
    public class BundledAssetRequest : AssetRequest
    {
        public BundledAssetRequest(IAssetInfo assetInfo, IAssetGraph assetGraph)
            : base(assetInfo, assetGraph)
        { }

        protected override void OnExecute()
        {
            string path = _assetInfo.Path.Substring(5);
            string[] parts = path.Split('/');

            string bundleKey = parts[0];
            string resKey = parts[1];

            var node = _assetGraph.GetNode(bundleKey);

            if (node == null)
            {
                TheResponse = new Response<Object>()
                {
                    Type = Response.ResponseTypes.Failure,
                    ErrorMessage = "Can't find asset bundle: " + bundleKey
                };
                return;
            }

            AssetBundle ab = node.Value as AssetBundle;
            if (ab == null)
            {
                TheResponse = new Response<Object>()
                {
                    Type = Response.ResponseTypes.Failure,
                    ErrorMessage = bundleKey + " is not asset bundle, type: " + node.Value.GetType().Name
                };
                return;
            }

            var obj = ab.LoadAsset(resKey);
            if (obj == null)
            {
                TheResponse = new Response<Object>()
                {
                    Type = Response.ResponseTypes.Failure,
                    ErrorMessage = bundleKey + " doesn't contain " + resKey
                };
                return;
            }

#if UNITY_EDITOR
            FixMaterials(obj);
#endif
            TheResponse = new Response<Object>() { Data = obj };
        }

        protected override bool OnUpdate()
        {
            return true;
        }

        protected override void OnDispose()
        {
            base.OnDispose();
            _assetInfo = null;
        }

#if UNITY_EDITOR

        private void FixMaterials(Object obj)
        {
            GameObject go = null;
            if (obj is Component)
                go = (obj as Component).gameObject;
            else if (obj is GameObject)
                go = obj as GameObject;

            if (go == null)
                return;

            Renderer[] renderers = go.GetComponentsInChildren<Renderer>();

            for (int i = 0; i < renderers.Length; i++)
            {
                var mats = renderers[i].materials;
                for (int j = 0; j < mats.Length; j++)
                    mats[j].shader = Shader.Find(mats[j].shader.name);
            }
        }

#endif

    }
}