﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using System.Text;

using Observable.Requests;

namespace AssetManagement.Requests
{
    public class TextAssetRequest : WebAssetRequest
    {
        public TextAssetRequest(IAssetInfo assetInfo, IAssetGraph assetGraph, string url)
            : base(assetInfo, assetGraph, url)
        { }

        protected override bool OnUpdate()
        {
            bool isBaseDone = base.OnUpdate();
            if (true == isBaseDone)
            {
                if (null != _bytes)
                {
                    string encoded = Encoding.UTF8.GetString(_bytes);
                    TheResponse = new Response<Object>() { Data = RuntimeTextAsset.Create(encoded) };
                }

                return true;
            }

            return false;
        }
    }

    public class RuntimeTextAsset : TextAsset
    {
        new public string text { get; private set; }

        public static RuntimeTextAsset Create(string text)
        {
            var result = new RuntimeTextAsset();
            result.text = text;

            return result;
        }
    }
}
