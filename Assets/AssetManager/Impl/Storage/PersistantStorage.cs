﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;

using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AssetManagement
{
    public class PersistantStorage : IStorage
    {
        private const string MAP_FILENAME = "global_data_cachedata.json";
        private const string DATA_FOLDER = "global_data";

        private static PersistantStorage _current;
        public static PersistantStorage Current
        {
            get
            {
                if (null == _current)
                    _current = new PersistantStorage(MAP_FILENAME, DATA_FOLDER);

                return _current;
            }
        }

        private string _mapFilename;
        private string _dataFolder;

        private Dictionary<string, int> _cache;

        public bool Enabled
        {
            get { return true; }
        }

        private PersistantStorage(string mapFilename, string dataFolderName)
        {
            _mapFilename = mapFilename;
            _dataFolder = dataFolderName;
            Load();
        }

        /// 
        /// API
        /// 

        public T Load<T>(string key)
        {
            T result = default(T);

            byte[] data = Load(key);
            string ser = Encoding.UTF8.GetString(data);

            result = Serializer.Current.DeSerializeFromString<T>(ser);
            return result;
        }

        public byte[] Load(string key)
        {
            if (false == Enabled)
            {
                return null;
            }

            string path = GetItemPath(key);
            if (false == File.Exists(path))
            {
                return null;
            }

            byte[] result;
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                result = ReadFully(fs);
            }

            return result;
        }

        public void Save<T>(string key, T data, int version)
        {
            if (false == Enabled)
            {
                Debug.LogWarning("Cache is disabled!");
                return;
            }

            string str = Serializer.Current.SerializeToString(data);
            byte[] res = Encoding.UTF8.GetBytes(str);
            Save(key, res, version);
        }

        public void Save(string key, byte[] data, int version)
        {
            if (false == Enabled)
            {
                Debug.LogWarning("Cache is disabled!");
                return;
            }

            string path = GetItemPath(key);
            if (true == File.Exists(path))
            {
                File.Delete(path);
            }

            string dir = Path.GetDirectoryName(path);
            if (false == Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                fs.Write(data, 0, data.Length);
                fs.Flush();
            }

            _cache[key] = version;

            Save();
        }

        public bool Contains(string name)
        {
            return ContainsVersion(name) > -1;
        }

        public void Cleanup()
        {
            foreach (var key in _cache.Keys)
            {
                string path = GetItemPath(key);
                if (true == File.Exists(path))
                {
                    File.Delete(path);
                }
                else
                {
                    Debug.LogWarning("Data cache has been not detected!");
                }
            }

            string cachePath = GetMapPath();

            if (File.Exists(cachePath))
            {
                File.Delete(cachePath);
            }
            else
            {
                Debug.LogWarning("Cache data has been not detected!");
            }
        }

        public int ContainsVersion(string key)
        {
            int result;
            if (Enabled && _cache.TryGetValue(key, out result))
            {
                return result;
            }

            return -1;
        }

        /// 
        /// INTERNAL
        /// 

        private void Load()
        {
            string mapPath = GetMapPath();

            if (true == File.Exists(mapPath))
            {
                _cache = Serializer.Current.DeSerializeFromFile<Dictionary<string, int>>(mapPath);
            }

            if (null == _cache)
            {
                _cache = new Dictionary<string, int>();
            }
        }

        private void Save()
        {
            string mapPath = GetMapPath();

            if (true == File.Exists(mapPath))
            {
                File.Delete(mapPath);
            }

            Serializer.Current.SerializeToFile(_cache, mapPath);
        }

        private string GetMapPath()
        {
            return Path.Combine(Application.persistentDataPath, _mapFilename);
        }

        private string GetItemPath(string key)
        {
            string dir = Path.Combine(Application.persistentDataPath, _dataFolder);
            return Path.Combine(dir, key);
        }

        private static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
