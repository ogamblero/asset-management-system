﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using System.Text;

namespace AssetManagement
{
    public class PrefsStorage : IStorage
    {
        private static PrefsStorage _current;
        public static PrefsStorage Current
        {
            get
            {
                if (null == _current)
                    _current = new PrefsStorage();

                return _current;
            }
        }

        private PrefsStorage()
        { }

        /// 
        /// API
        /// 

        public T Load<T>(string key)
        {
            T result = default(T);

            if (false == PlayerPrefs.HasKey(key))
            {
                return result;
            }

            string ser = PlayerPrefs.GetString(key);
            if (true == string.IsNullOrEmpty(ser))
            {
                return result;
            }

            result = Serializer.Current.DeSerializeFromString<T>(ser);

            return result;
        }

        public byte[] Load(string key)
        {
            byte[] result = null;
            if (false == PlayerPrefs.HasKey(key))
            {
                return result;
            }

            string ser = PlayerPrefs.GetString(key);
            if (true == string.IsNullOrEmpty(ser))
            {
                return result;
            }

            result = Encoding.UTF8.GetBytes(ser);

            return result;
        }

        public void Save<T>(string key, T data, int version)
        {
            string ser = Serializer.Current.SerializeToString(data);

            if (true == PlayerPrefs.HasKey(key))
            {
                PlayerPrefs.DeleteKey(key);
            }

            PlayerPrefs.SetString(key, ser);
            PlayerPrefs.Save();
        }

        public void Save(string key, byte[] data, int version)
        {
            string str = new string(Encoding.UTF8.GetChars(data));

            if (true == PlayerPrefs.HasKey(key))
            {
                PlayerPrefs.DeleteKey(key);
            }
            PlayerPrefs.SetString(key, str);
            PlayerPrefs.Save();
        }

        public void Cleanup()
        {
            PlayerPrefs.DeleteAll();
        }

        public int ContainsVersion(string key)
        {
            return PlayerPrefs.HasKey(key) ? 0 : -1;
        }

        public bool Contains(string name)
        {
            return ContainsVersion(name) > -1;
        }
    }
}