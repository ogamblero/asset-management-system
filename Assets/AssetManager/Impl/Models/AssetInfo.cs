﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Collections.Generic;

namespace AssetManagement.Models
{
    [Serializable]
    public class AssetInfo : IAssetInfo
    {
        public string key;
        public string path;
        public int hash;
        public List<string> deps;

        public string Key { get { return key; } }
        public string Path { get { return path; } }
        public int Hash { get { return hash; } }
        public IEnumerable<string> Dependencies { get { return deps; } }
    }
}
