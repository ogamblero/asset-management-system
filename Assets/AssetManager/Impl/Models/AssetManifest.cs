﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Collections.Generic;

namespace AssetManagement.Models
{
    [Serializable]
    public class AssetManifest
	{
        public int Version;
        public List<AssetInfo> Assets;

        public AssetManifest()
		{
			Assets = new List<AssetInfo> ();
        }
    }
}