﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Collections.Generic;

using UnityObject = UnityEngine.Object;

namespace AssetManagement
{
    public interface IAssetManager
    {
        ILoadingToken Load(IEnumerable<string> keys);
        ILoadingToken Load(string key);

        void Evaluate<T>(string key, Action<string, T> callback) where T : UnityObject;
        bool TryEvaluate<T>(string key, out T obj) where T : UnityObject;
    }
}