﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using System;

namespace AssetManagement
{
    public interface ILoadingToken
    {
        event Action<ILoadingToken> StateChanged;

        LoadingStates State { get; }
        float Progress { get; }

        void Cancel();
        void Retry();
    }

    public enum LoadingStates
    {
        Running,
        Succeed,
        Failured
    }
}