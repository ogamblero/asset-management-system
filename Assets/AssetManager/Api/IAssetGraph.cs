﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using System.Collections.Generic;

namespace AssetManagement
{
    public interface IAssetInfo
    {
        string Key { get; }
        string Path { get; }
        int Hash { get; }
        IEnumerable<string> Dependencies { get; }
    }

    public interface IAssetNode
    {
        float LoadingProgress { get; }
        NodeStates State { get; }

        Object Value { get;}
        IAssetInfo AssetInfo { get; }

        IEnumerable<IAssetNode> Dependencies { get; }
        IEnumerable<IAssetNode> Depended { get; }
    }

    public interface IAssetGraph
    {
        IEnumerable<IAssetNode> Nodes { get; }
        IAssetNode GetNode(string key);
    }

    public enum NodeStates : byte
    {
        Idle,
        Loading,
        Loaded,
        Failed
    }
}

