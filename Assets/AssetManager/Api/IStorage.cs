﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

namespace AssetManagement
{
    public interface IStorage
    {
        T Load<T>(string key);
        byte[] Load(string key);

        void Save<T>(string key, T data, int version);
        void Save(string key, byte[] data, int version);

        int ContainsVersion(string key);

        void Cleanup();
    }
}
