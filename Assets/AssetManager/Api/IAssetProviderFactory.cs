﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

namespace AssetManagement
{
    public interface IAssetProviderFactory
    {
        AssetRequest CreateProvider(IAssetInfo assetInfo, IAssetGraph graph);
    }
}
