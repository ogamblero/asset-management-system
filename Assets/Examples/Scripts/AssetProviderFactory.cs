﻿using UnityEngine;
using UnityEngine.Assertions;

using System;
using System.Collections.Generic;
using System.Linq;

using AssetManagement;
using AssetManagement.Models;
using AssetManagement.Requests;
using Observable.Requests;

using UnityObject = UnityEngine.Object;

public class AssetProviderFactory : MonoBehaviour, IAssetProviderFactory
{
    public AssetRequest CreateProvider(IAssetInfo assetInfo, IAssetGraph graph)
    {
        if (assetInfo.Path.EndsWith(".bundle"))
        {
            string url = Application.dataPath + "/Examples/dlc_export/Android/" + assetInfo.Path;

            if (url.StartsWith("http://"))
                return new AssetManagement.Requests.AssetBundleRequest(assetInfo, graph, url);
            else
                return new LocalAssetBundleRequest(assetInfo, graph, url);
        }

        if (assetInfo.Path.StartsWith("ab://"))
            return new BundledAssetRequest(assetInfo, graph);

        if (assetInfo.Path.EndsWith(".json") || assetInfo.Path.EndsWith(".txt"))
            return new TextAssetRequest(assetInfo, graph, assetInfo.Path);

        throw new NotImplementedException();
    }
}

