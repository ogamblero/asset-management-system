﻿using UnityEngine;
using System;
using System.Collections.Generic;

using AssetManagement;
using AssetManagement.Models;

public class AssetManagerClient : MonoBehaviour
{
    [SerializeField] string embededManifestPath = "embeded/emb_manifest";
    [SerializeField] string dlcManifestPath = "dlc/dlc_manifest_Android";

    [SerializeField] string embAssetKey = "emb_cube_01";
    [SerializeField] string dlcAssetKey = "dlc_cube_01_mat";

    private AssetManager _assetManager;
    private ILoadingToken _loadingToken;

	void Start ()
    {
        var embededManifest = LoadManifest(embededManifestPath);
        var dlcManifest = LoadManifest(dlcManifestPath);

        var aProviderFactory = GetComponent<IAssetProviderFactory>();

        _assetManager = AssetManager.Create(dlcManifest, embededManifest, aProviderFactory);

        _loadingToken = _assetManager.Load(new List<string> { embAssetKey, dlcAssetKey });

        _loadingToken.StateChanged += LoadingToken_StateChanged;
        LoadingToken_StateChanged(_loadingToken);

        _assetManager.Evaluate<UnityEngine.Object>(embAssetKey, AssetManager_EvaluateHandler);
        _assetManager.Evaluate<UnityEngine.Object>(dlcAssetKey, AssetManager_EvaluateHandler);
    }

    private void LoadingToken_StateChanged(ILoadingToken sender)
    {
        if (sender.State == LoadingStates.Failured)
        {
            Debug.LogError("Loading was failed!");
            return;
        }

        if (sender.State == LoadingStates.Succeed)
        {
            Debug.Log("Loading was finished!");
        }
    }

    private void AssetManager_EvaluateHandler(string key, UnityEngine.Object obj)
    {
        if (obj != null)
            Debug.LogFormat(" {0} has been evaluated.", key);
        else
            Debug.LogErrorFormat("Can't evaluate {0} asset.", key);
    }

    private AssetManifest LoadManifest(string path)
    {
        var asset = Resources.Load(path, typeof(TextAsset)) as TextAsset;
        if (asset == null)
        {
            Debug.LogError("Embedded manifest could not be loaded.");
            return null;
        }

        try
        {
            var manifest = Serializer.Current.DeSerializeFromString<AssetManifest>(asset.text);
            return manifest;
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }

        return null;
    }

    private void Update()
    {
        if (_loadingToken != null)
        {
            var progress = _loadingToken.Progress;
            Debug.LogFormat("Loading progress: {0}", progress);
            if (progress > 0.99f)
                _loadingToken = null;
        }
    }
}
