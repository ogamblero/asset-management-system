﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using System.Collections.Generic;

namespace Observable.Requests
{
    public abstract class WebRequest : Request<object>
    {
        public const float TIMEOUT = 15.0f; // sec

        protected WWW _www;
        private float _startTime;

        private string _url;
        private Dictionary<string, string> _post;

        public override float Progress
        {
            get { return _www != null ? _www.progress : 0.0f; }
        }

        public WebRequest()
        { }
        public WebRequest(string url)
        {
            _url = url;
        }
        public WebRequest(string url, Dictionary<string, string> post)
            : this(url)
        {
            _post = post;
        }

        protected override void OnExecute()
        {
            _startTime = Time.realtimeSinceStartup;

            if (null == _post)
            {
                _www = new WWW(_url);
            }
            else
            {
                var form = new WWWForm();
                foreach (var pair in _post)
                {
                    form.AddField(pair.Key, pair.Value);
                }
                _www = new WWW(_url, form);
            }
        }

        protected override bool OnUpdate()
        {
            if (null != _www && (Time.realtimeSinceStartup - _startTime) > TIMEOUT)
            {
                _www.Dispose();
                _www = null;

                TheResponse.ErrorMessage = "Timeout";
                TheResponse.ErrorCode = 408;
                return true;
            }

            if (null != _www && _www.isDone)
            {
                if (true == string.IsNullOrEmpty(_www.error))
                {
                    TheResponse.Type = Response.ResponseTypes.Success;
                    TheResponse.Data = OnResponse();
                }

                _www.Dispose();
                _www = null;
                return true;
            }

            return false;
        }

        protected virtual object OnResponse()
        {
            return _www.text;
        }

        protected override void OnDispose()
        {
            if (null != _www)
            {
                _www.Dispose();
                _www = null;
            }
        }
    }

    public class WebGetRequest : WebRequest
    {
        public WebGetRequest(string url)
            : base(url)
        { }
    }

    public class WebPostRequest : WebRequest
    {
        public WebPostRequest(string url, Dictionary<string, string> post)
            : base(url, post)
        { }
    }
}
