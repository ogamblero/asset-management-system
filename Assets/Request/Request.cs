﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using System;

namespace Observable.Requests
{
    public abstract class Request : CustomYieldInstruction, IDisposable
    {
        protected Response _response;
        public virtual Response TheResponse
        {
            get { return _response; }
            protected set { _response = value; }
        }

        public virtual float Progress { get { return 1.0f; } }
        public override bool keepWaiting { get { return false == Update(); } }

        private Action<Request> _callback;

        public void Execute(Action<Request> callback)
        {
            _callback = callback;

            OnExecute();
            RequestManager.Current.AddRequest(this);
        }
        public void Dispose()
        {
            OnDispose();
        }

        protected virtual void OnExecute()
        { }
        protected virtual bool OnUpdate()
        { return true; }
        protected virtual void OnDispose()
        { }

        private bool Update()
        {
            bool isDone = OnUpdate();

            if (isDone)
            {
                if (_callback != null)
                    _callback(this);

                _callback = null;
            }
            return isDone;
        }
    }

    public abstract class Request<TResponse> : Request where TResponse : class
    {
        public new Response<TResponse> TheResponse
        {
            get { return _response as Response<TResponse>; }
            protected set { _response = value; }
        }
    }

    public class Response
    {
        public ResponseTypes Type { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public enum ResponseTypes : byte
        {
            Success = 0,
            Failure = 1
        }
    }

    public class Response<TData> : Response
    {
        public TData Data { get; set; }
    }
}
