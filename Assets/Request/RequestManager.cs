﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using System.Collections.Generic;
using Observable.Requests;

public class RequestManager : MonoBehaviour
{
    private static object syncRoot = new Object();
    private static volatile RequestManager _instance = null;

    private static bool _isQuiting;

    private readonly List<Request> _requests = new List<Request>();

    public static RequestManager Current
    {
        get
        {
            if (_instance == null && false == _isQuiting)
            {
                lock (syncRoot)
                {
                    _instance = FindObjectOfType(typeof(RequestManager)) as RequestManager;
                    if (null == _instance)
                    {
                        _instance = new GameObject("_requestManager", typeof(RequestManager)).GetComponent<RequestManager>();
                        DontDestroyOnLoad(_instance.gameObject);
                    }
                }
            }
            return _instance;
        }
    }
    public static bool Exists
    {
        get { return null != _instance; }
    }

    public void AddRequest(Request request)
    {
        _requests.Add(request);
    }
    public void RemoveRequest(Request request)
    {
        _requests.Remove(request);
    }

    private void Awake()
    {
        _instance = this;
    }

    private void Update()
    {
        for (int i = 0; i < _requests.Count; i++)
        {
            var keepWaiting = _requests[i].keepWaiting;
            if (false == keepWaiting)
            {
                RemoveRequest(_requests[i]);
                break;
            }
        }
    }

    void OnApplicationQuit()
    {
        _isQuiting = true;
        _instance = null;
    }
}
